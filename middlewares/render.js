var logger = require('../common/logger');

// Patch res.render method to output logger
exports.render = function (req, res, next) {
  res._render = res.render;

  //注意这里获取render时间的方式，先copy一个render方法，在express的render方法中用Date对象记录render时间。
  res.render = function (view, options, fn) {
    var t = new Date();

    res._render(view, options, fn);

    var duration = (new Date() - t);
    logger.info("Render view", view, ("(" + duration + "ms)").green);
  };

  next();
};
