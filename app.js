/*!
 * nodeclub - app.js
 */

/**
 * Module dependencies.
 */

var config = require('./config');

//在生产环境下，定义oneapm的时候使用它来监视性能
if (!config.debug && config.oneapm_key) {
  require('oneapm');
}

require('colors');
var path = require('path');
var Loader = require('loader');
var LoaderConnect = require('loader-connect');
var express = require('express');

//connect-redis is a Redis session store backed by node_redis, and is insanely fast :). Requires redis >= 2.0.0 for the SETEX command.
//
var session = require('express-session');
var passport = require('passport');
require('./middlewares/mongoose_log'); // 打印 mongodb 查询日志
require('./models');
var GitHubStrategy = require('passport-github').Strategy;
var githubStrategyMiddleware = require('./middlewares/github_strategy');
var webRouter = require('./web_router');
var apiRouterV1 = require('./api_router_v1');
var auth = require('./middlewares/auth');
var errorPageMiddleware = require('./middlewares/error_page');
var proxyMiddleware = require('./middlewares/proxy');
var RedisStore = require('connect-redis')(session);
var _ = require('lodash');
var csurf = require('csurf');
var compress = require('compression');
var bodyParser = require('body-parser');
var busboy = require('connect-busboy');
var errorhandler = require('errorhandler');
var cors = require('cors');
var requestLog = require('./middlewares/request_log');
var renderMiddleware = require('./middlewares/render');
var logger = require('./common/logger');
var helmet = require('helmet');
var bytes = require('bytes')


// 静态文件目录
var staticDir = path.join(__dirname, 'public');
// assets
var assets = {};

//生产环境中压缩代码
if (config.mini_assets) {
  try {
    assets = require('./assets.json');
  } catch (e) {
    logger.error('You must execute `make build` before start app when mini_assets is true.');
    throw e;
  }
}

//url模块的功能是把url专程key-value的对象
var urlinfo = require('url').parse(config.host);
config.hostname = urlinfo.hostname || config.host;

var app = express();

// configuration in all env
//设置html模版目录位置，
app.set('views', path.join(__dirname, 'views'));

//express中的view engine实际上是指定express的模版文件后缀
app.set('view engine', 'html');
//engine方法实际上是指定特定后缀文件的渲染引擎
app.engine('html', require('ejs-mate'));

//ejs的母版页，使用了<% layout('layout.html') -%>的页面，页面内容都会在<%-body %>中
app.locals._layoutFile = 'layout.html';

//一般鱼nginx反向代理相关的配置
app.enable('trust proxy');

//log中间件放到所有中间件活着路由的前面，每一个请求到来的时候都要记录请求的日志
// Request logger。请求时间
app.use(requestLog);

//在测试环境下启用访问时间显示，方便查看性能
if (config.debug) {
  // 显示渲染时间的中间件
  app.use(renderMiddleware.render);
}

// 静态资源
if (config.debug) {
  //loader-connect的作用是在开发环境中实时的编译less，babel等文件，方便开发调试，注意不要在生产环境中使用，会影响效率
  app.use(LoaderConnect.less(__dirname)); // 测试环境用，编译 .less on the fly
}

//express提供静态资源文件目录的方式
app.use('/public', express.static(staticDir));

//app.use使用中间件，注意理解app.get/app.post本质上也是中间件
//意义是对一些路由货api进行访问限制，只有特定的域名可以访问
app.use('/agent', proxyMiddleware.proxy);

// 通用的中间件
//Create a middleware that adds a X-Response-Time header to responses.
//在header中写入访问的时间
app.use(require('response-time')());

//Helmet helps you secure your Express apps by setting various HTTP headers. It's not a silver bullet, but it can help!
//在http header中写入一些安全的申明项
app.use(helmet.frameguard('sameorigin'));

//Parse incoming request bodies in a middleware before your handlers, available under the req.body property.
//在路由controller之前获取http请求参数，并限制最大参数
app.use(bodyParser.json({limit: '1mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '1mb' }));

//Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it.
app.use(require('method-override')());

//用cookie中间件。
app.use(require('cookie-parser')(config.session_secret));

//对返回的数据进行压缩
app.use(compress());

//Create a session middleware with the given options.
//Note Session data is not saved in the cookie itself, just the session ID. Session data is stored server-side.
//express-session和cookie-parser使用的secret必须一致
app.use(session({
  secret: config.session_secret,
  store: new RedisStore({
    port: config.redis_port,
    host: config.redis_host,
    db: config.redis_db,
    pass: config.redis_password,
  }),
  resave: false,
  saveUninitialized: false,
}));

// oauth 中间件
app.use(passport.initialize());

// github oauth
passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (user, done) {
  done(null, user);
});
passport.use(new GitHubStrategy(config.GITHUB_OAUTH, githubStrategyMiddleware));

// custom middleware
app.use(auth.authUser);
app.use(auth.blockUser());

if (!config.debug) {
  app.use(function (req, res, next) {
    if (req.path === '/api' || req.path.indexOf('/api') === -1) {
      csurf()(req, res, next);
      return;
    }
    next();
  });
  app.set('view cache', true);
}

// for debug
// app.get('/err', function (req, res, next) {
//   next(new Error('haha'))
// });

// set static, dynamic helpers
_.extend(app.locals, {
  config: config,
  Loader: Loader,
  assets: assets
});

app.use(errorPageMiddleware.errorPage);
_.extend(app.locals, require('./common/render_helper'));
app.use(function (req, res, next) {
  res.locals.csrf = req.csrfToken ? req.csrfToken() : '';
  next();
});

app.use(busboy({
  limits: {
    fileSize: bytes(config.file_limit)
  }
}));

// routes
app.use('/api/v1', cors(), apiRouterV1);
app.use('/', webRouter);

// error handler
if (config.debug) {
  app.use(errorhandler());
} else {
  app.use(function (err, req, res, next) {
    logger.error(err);
    return res.status(500).send('500 status');
  });
}

if (!module.parent) {
  app.listen(config.port, function () {
    logger.info('NodeClub listening on port', config.port);
    logger.info('God bless love....');
    logger.info('You can debug your app with http://' + config.hostname + ':' + config.port);
    logger.info('');
  });
}

module.exports = app;
