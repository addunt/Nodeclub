var config = require('../config');

//如果没指定node的环境类型，默认dev环境
var env = process.env.NODE_ENV || "development"

//log4js是一个nodejs下的日志辅助工具，能够方便的输出和nginx日志格式统一的信息完整的日志。
var log4js = require('log4js');
log4js.configure({
  appenders: [
    { type: 'console' },
    { type: 'file', filename: 'logs/cheese.log', category: 'cheese' }
  ]
});

var logger = log4js.getLogger('cheese');
logger.setLevel(config.debug && env !== 'test' ? 'DEBUG' : 'ERROR')

module.exports = logger;
