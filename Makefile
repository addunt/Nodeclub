TESTS = $(shell find test -type f -name "*.test.js")
TEST_TIMEOUT = 10000
MOCHA_REPORTER = spec
# NPM_REGISTRY = "--registry=http://registry.npm.taobao.org"
NPM_REGISTRY = ""


all: test

install:
	#npm install
	@npm install $(NPM_REGISTRY)

pretest:
	#创建config.js文件盒public目录
	@if ! test -f config.js; then \
		cp config.default.js config.js; \
	fi
	@if ! test -d public/upload; then \
		mkdir public/upload; \
	fi

test: install pretest
	#mocha事javascript的测试框架
	@NODE_ENV=test ./node_modules/mocha/bin/mocha \
		--reporter $(MOCHA_REPORTER) \
		-r should \
		-r test/env \
		--timeout $(TEST_TIMEOUT) \
		$(TESTS)

testfile:
	@NODE_ENV=test ./node_modules/mocha/bin/mocha \
		--reporter $(MOCHA_REPORTER) \
		-r should \
		-r test/env \
		--timeout $(TEST_TIMEOUT) \
		$(FILE)

test-cov cov: install pretest
	#Istanbul 是JavaScript 程序的代码覆盖率工具
	@NODE_ENV=test node \
		node_modules/.bin/istanbul cover --preserve-comments \
		./node_modules/.bin/_mocha \
		-- \
		-r should \
		-r test/env \
		--reporter $(MOCHA_REPORTER) \
		--timeout $(TEST_TIMEOUT) \
		$(TESTS)


build:
	#loader-builder是对less，coffee，babel等代码进行编译的工具
	#这里用的目的应该是压缩js和css的代码体积，将要压缩的代码所在路径views指定
	@./node_modules/loader-builder/bin/builder views .

run:
	@node app.js

start: install build
	#在build的情况下，更改NODE_ENV的值为production，然后用pm2来管理node的进程
	@NODE_ENV=production ./node_modules/.bin/pm2 start app.js -i 0 --name "cnode" --max-memory-restart 400M

restart: install build
	@NODE_ENV=production ./node_modules/.bin/pm2 restart "cnode"

.PHONY: install test testfile cov test-cov build run start restart
